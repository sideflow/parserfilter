package com.antropix.matcher.ria;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.antropix.matcher.domain.User;
import com.antropix.matcher.domain.User.Properties;
import com.antropix.matcher.domain.User.Roles;
import com.antropix.matcher.filter.Filter;
import com.antropix.matcher.filter.FilterFactory;
import com.antropix.matcher.filter.configuration.StringConfiguration;
import com.antropix.matcher.query.predicate.Is;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.Sizeable;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;

public class QuizApplicationUIBuilder implements ClickListener {

	private static final long serialVersionUID = -6004389862348550230L;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(QuizApplicationUIBuilder.class);

	private static final String USERS_TREE_TITLE = "Users";

	private static final String QUERY_AREA_TITLE = "Query";

	private static final String USERS_TABLE_TITLE = "Users matching query";

	private static final String GREETINGS = "Welcome to the interactive demo for the object filtering assessment.";

	private static final String PROJ_SRC = "https://bitbucket.org/sideflow/parserfilter/src";

	private static final Object USER_PROPERTY_NAME = "USER_TREE_ITEM";

	private String SRC_LINK_TIP = "Source of this application located here.";
	private String SRC_LINK_CAPTION = "Get the source.";
	
	private String DEFAULT_QUERY = "(((role = \"administrator\" or role = \"developer\" or role=\"director\") or "+
									" (surname = \"Bloggs\")) and (age < \"38\")) ";

	private Collection<User> users;

	private Tree usersTree;
	private TextArea queryArea;
	private Button filterButton;
	private Table usersTable;

	public VerticalSplitPanel buildRootPanel(Collection<User> users) {
		setUsers(users);
		VerticalSplitPanel rootPanel = new VerticalSplitPanel();
		rootPanel.setLocked(true);
		rootPanel.setSplitPosition(10, Sizeable.UNITS_PERCENTAGE);

		rootPanel.addComponent(buildHeaderLayout());
		rootPanel.addComponent(buildUsersListPanel());

		return rootPanel;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		String queryString = (String) queryArea.getValue();
		if (!StringUtils.isEmpty(queryString)) {
			filterUsingQuery(queryString);
		}
		else {
			usersTable.setContainerDataSource(getUsersTableContainer(getUsers()));
		}
	}


	// Getters & setters

	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}

	//
	// Private block
	//
	
	private void filterUsingQuery(String queryString) {
		Filter<User> usersFilter = FilterFactory
				.createFilter(new StringConfiguration(queryString));

		LOGGER.info("Filtering users on query {}", queryString);

		Collection<User> results = usersFilter.findMatches(getUsers());
		usersTable.setContainerDataSource(getUsersTableContainer(results));
		LOGGER.info("Filtering results shown in users' table.");
	}

	private VerticalLayout buildLeftLayout() {
		VerticalLayout leftLayout = buildVerticalLayout();
		usersTree = buildUsersTree();
		leftLayout.addComponent(usersTree);
		LOGGER.debug("built left layout.");

		return leftLayout;
	}

	private VerticalLayout buildMainLayout() {
		VerticalLayout mainLayout = buildVerticalLayout();

		buildQueryArea();
		buildFilterButtton();
		buildUsersTable();

		addTopLeft(mainLayout, queryArea);
		addTopLeft(mainLayout, filterButton);
		addTopLeft(mainLayout, usersTable);
		mainLayout.setExpandRatio(usersTable, 1.f);
		
		filterUsingQuery((String) queryArea.getValue());
		
		LOGGER.debug("built main layout.");

		return mainLayout;
	}

	private void addTopLeft(VerticalLayout layout, Component component) {
		layout.addComponent(component);
		layout.setComponentAlignment(component, Alignment.TOP_LEFT);
	}

	private void buildFilterButtton() {
		filterButton = new Button("Filter...");
		filterButton.addListener(this);
		LOGGER.debug("built filter button.");
	}

	private void buildUsersTable() {
		usersTable = new Table(USERS_TABLE_TITLE);
		usersTable.setColumnHeader(USER_PROPERTY_NAME, 
								   User.Properties.FIRSTNAME.getName() + " " +
								   User.Properties.SURNAME.getName());
		usersTable.setSizeFull();
		usersTable.setImmediate(true);
		usersTable.setContainerDataSource(getUsersTableContainer(getUsers()));
		LOGGER.debug("built users' table.");
	}

	private void buildQueryArea() {
		queryArea = new TextArea(QUERY_AREA_TITLE);
		queryArea.setRows(5);
		queryArea.setColumns(30);
		queryArea.setValue(DEFAULT_QUERY);
		LOGGER.debug("built query area.");
	}

	private VerticalLayout buildVerticalLayout() {
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		return mainLayout;
	}

	private Tree buildUsersTree() {
		Tree usersTree = new Tree(USERS_TREE_TITLE);
		usersTree.setItemCaptionPropertyId(USER_PROPERTY_NAME);
		usersTree.setContainerDataSource(getUsersTreeContainer(getUsers()));
		// Expand whole tree
        for (Object id : usersTree.rootItemIds()) {
            usersTree.expandItemsRecursively(id);
        }
		LOGGER.debug("built users' tree.");

		return usersTree;
	}

	private HorizontalSplitPanel buildUsersListPanel() {
		HorizontalSplitPanel usersListPanel = new HorizontalSplitPanel();
		usersListPanel.setLocked(true);
		usersListPanel.setSplitPosition(20, Sizeable.UNITS_PERCENTAGE);
		VerticalLayout leftLayout = buildLeftLayout();
		VerticalLayout mainLayout = buildMainLayout();

		mainLayout.setSpacing(false);
		mainLayout.setMargin(false);

		usersListPanel.addComponent(leftLayout);
		usersListPanel.addComponent(mainLayout);
		LOGGER.debug("built users list panel.");

		return usersListPanel;
	}

	private HorizontalLayout buildHeaderLayout() {
		Label greetingLabel = new Label(GREETINGS);
		Link srcLink = new Link(SRC_LINK_CAPTION,
				new ExternalResource(PROJ_SRC));
		srcLink.setDescription(SRC_LINK_TIP);
		HorizontalLayout headerLayout = new HorizontalLayout();
		headerLayout.setMargin(false, false, false, true);
		headerLayout.setSizeFull();
		headerLayout.addComponent(greetingLabel);
		headerLayout.setComponentAlignment(greetingLabel, Alignment.MIDDLE_LEFT);
		headerLayout.addComponent(srcLink);
		headerLayout.setComponentAlignment(srcLink, Alignment.MIDDLE_LEFT);
		LOGGER.debug("built header.");

		return headerLayout;
	}

	public HierarchicalContainer getUsersTreeContainer(Collection<User> users) {
		Item item = null;

		HierarchicalContainer userContainer = new HierarchicalContainer();
		userContainer.addContainerProperty(USER_PROPERTY_NAME, String.class,
				null);

		int itemId = 0;
		for (Roles role : Roles.values()) {
			int parentId = itemId;
			item = userContainer.addItem(itemId);
			item.getItemProperty(USER_PROPERTY_NAME).setValue(role.getName());

			userContainer.setChildrenAllowed(itemId, true);
			itemId++;

			String queryString = Properties.ROLE.getName() + Is.getSign()
					+ role.getQuotedName();
			Filter<User> roleFilter = FilterFactory
					.createFilter(new StringConfiguration(queryString));

			for (User userWithRole : roleFilter.findMatches(users)) {
				item = userContainer.addItem(itemId);
				item.getItemProperty(USER_PROPERTY_NAME).setValue(
						userWithRole.getFirstname() + " "
								+ userWithRole.getSurname() + " "
								+ userWithRole.getAge());

				userContainer.setChildrenAllowed(itemId, false);
				userContainer.setParent(itemId, parentId);
				itemId++;
			}
		}
		return userContainer;
	}

	private IndexedContainer getUsersTableContainer(Collection<User> users) {
		IndexedContainer container = new IndexedContainer();
		container.addContainerProperty(USER_PROPERTY_NAME, String.class, null);
		int itemId = 0;
		for (User user : users) {
			String name = user.getFirstname() + " " + user.getSurname();
			Item item = container.addItem(++itemId);
			item.getItemProperty(USER_PROPERTY_NAME).setValue(name);
		}
		container.sort(new Object[] { USER_PROPERTY_NAME },
				new boolean[] { true });

		return container;
	}

}
