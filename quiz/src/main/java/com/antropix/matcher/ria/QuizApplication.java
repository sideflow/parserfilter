package com.antropix.matcher.ria;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.antropix.matcher.domain.User;
import com.vaadin.Application;
import com.vaadin.ui.Window;

/**
 * 
 * @author aheddad
 *
 */
public class QuizApplication extends Application {


	private static final long serialVersionUID = 7554853137664673466L;

	
	
	
	@Override
	public void init() {
		
		Set<User> users = new HashSet<User>(Arrays.asList(new User("Joe", "Bloggs", User.Roles.ADMINISTRATOR, "35"),
						   new User("John", "Bloggs", User.Roles.ADMINISTRATOR, "35"),
						   new User("Suzie", "Wang", User.Roles.DEVELOPER, "29"),
						   new User("Linus", "Berg", User.Roles.MANAGER, "32"),
						   new User("Demitrios", "Popoulos", User.Roles.DIRECTOR, "37")));
		
		Window mainWindow = new Window("Quiz Application");
	    mainWindow.setContent(new QuizApplicationUIBuilder().buildRootPanel(users));
		setMainWindow(mainWindow);
	}





}
