package com.antropix.matcher.filter.configuration;

import java.util.List;

import com.antropix.matcher.filter.FilterFactory.FilterType;
import com.antropix.matcher.query.QueryTerm;

/**
 * FilterConfiguration that takes its configuration as a String.
 * 
 * @author aheddad
 *
 */
public class StringConfiguration implements FilterConfiguration {

	private String queryString;
	private FilterType type;

	public StringConfiguration(String queryString) {
		this.queryString = queryString;
		this.type = FilterType.STRING_QUERY;
	}

	public List<?> getAndProperties() {
		new QueryTerm(getQueryString());
		return null;
	}

	public String getQueryString() {
		return queryString;
	}

	public FilterType getType() {
		return type;
	}
	

}
