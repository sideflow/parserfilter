package com.antropix.matcher.filter.configuration;

import java.util.Collection;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import com.antropix.matcher.filter.FilterFactory.FilterType;

/**
 * 
 * @author aheddad
 *
 */
public class XMLFilterConfiguration implements FilterConfiguration {

	XMLConfiguration configuration;
	
	public XMLFilterConfiguration(String fileName) throws ConfigurationException {
		configuration = new XMLConfiguration(fileName);
	}

	/**
	 * Return all properties to AND
	 * @return
	 */
	public List<?> getAndProperties() {
		// TODO: match back properties to values should be a map
		// Object allProperties = configuration.getList("predicates.and.fields.field.name");
		Object allValues = configuration.getList("predicates.and.fields.field.value");

		if (allValues instanceof Collection) {
			return (List<?>)allValues;
		}
		return null;
	}

	@Override
	public FilterType getType() {
		return FilterType.XML;
	}
}
