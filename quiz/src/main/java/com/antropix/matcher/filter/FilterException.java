package com.antropix.matcher.filter;

/**
 * RuntimeException thrown if {@link Filter#matches(com.antropix.matcher.domain.User...)} went wrong.
 * 
 * @author aheddad
 *
 */
public class FilterException extends RuntimeException {


	public FilterException(String string) {
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = -2470588487614788922L;

}
