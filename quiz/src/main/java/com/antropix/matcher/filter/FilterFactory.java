package com.antropix.matcher.filter;

import com.antropix.matcher.filter.configuration.FilterConfiguration;
import com.antropix.matcher.filter.configuration.StringConfiguration;

/**
 * Factory for {@link Filter}s.
 * 
 * @author aheddad
 * 
 */
public class FilterFactory<T> {

	public enum FilterType {
		STRING_QUERY, XML;
	}
	
	private FilterFactory() {
	}

	/**
	 * Creates a {@link Filter} given its type.
	 * 
	 * @param configuration
	 *            the configuration to pass to the {@link Filter}
	 * @return The filter newly created.
	 */
	public static <T> Filter<T> createFilter(FilterConfiguration configuration) {
		switch (configuration.getType()) {
		case STRING_QUERY:
			return (Filter<T>) new FilterQueryMatcher((StringConfiguration) configuration);
		case XML:
			return null; // TODO FilterXMLMatcher
		default:
			throw new FilterException("You have not assigned your configuration with a type.");
		}

	}
 
}
