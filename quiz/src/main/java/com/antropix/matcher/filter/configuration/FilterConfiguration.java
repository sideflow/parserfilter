package com.antropix.matcher.filter.configuration;

import java.util.List;

import com.antropix.matcher.filter.Filter;
import com.antropix.matcher.filter.FilterFactory.FilterType;

/**
 * Configuration for a {@link Filter}.
 * 
 * @author aheddad
 * 
 */
public interface FilterConfiguration {

	/**
	 * TODO: might be removed.
	 * @return
	 */
	List<?> getAndProperties();

	/**
	 * Returns the type of the configuration.
	 * 
	 * @return the type of the configuration.
	 */
	FilterType getType();
}
