package com.antropix.matcher.filter;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.antropix.matcher.domain.User;
import com.antropix.matcher.filter.configuration.StringConfiguration;
import com.antropix.matcher.query.parser.ParseException;
import com.antropix.matcher.query.parser.UQLParser;

/**
 * Filter matcher implementation
 * 
 * @author aheddad
 *
 */
public class FilterQueryMatcher implements Filter<User> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FilterQueryMatcher.class);
	
	private Predicate parserMatch;
	
	FilterQueryMatcher(final StringConfiguration config) {
		this.parserMatch = createParserMatch(config);
	}

	
	@Override
	public boolean matches(User... users) {
		return this.matches(Arrays.asList(users));
	}
	
	@Override
	public boolean matches(Collection<User> users) {
		return CollectionUtils.exists(users, this.parserMatch);
	}

	@Override
	public Collection<User> findMatches(User... users) {
		return this.findMatches(Arrays.asList(users));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<User> findMatches(Collection<User> users) {
		return (Collection<User>) CollectionUtils.select(users, this.parserMatch);
	}
	
	
	// 
	// Private block
	//
	
	private Predicate createParserMatch(final StringConfiguration config) {
		return new Predicate() {
			public boolean evaluate(Object object) {
				User user = (User)object;
				
				String queryString = config.getQueryString();
				try {
					return UQLParser.matches(user, queryString);
				} catch (ParseException e) {
					LOGGER.error(e.getMessage());
					throw new FilterException("ParserException while matching "+
							user.toString()+
							" against query: "+
							queryString);
				}
			}
			
		};
	}
}
