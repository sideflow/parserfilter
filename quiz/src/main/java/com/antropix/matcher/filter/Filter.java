package com.antropix.matcher.filter;

import java.util.Collection;

/**
 * Interface to implement for filtering objects.
 *  
 * @author aheddad
 *
 */
public interface Filter<T> {
	
	/**
	 * True if some T in T:s matches using this {@link Filter}.
	 * 
	 * @param objects<T> the array of some type to match against this.
	 * @return true if some T in T:s matching using this {@link Filter}
	 */
	boolean matches(T... objects);
	
	/**
	 * True if some T in T:s matches using this {@link Filter}.
	 * 
	 * @param objects<T> the {@link Collection}<T> of some type to match against this.
	 * @return true if some T in T:s matching using this {@link Filter}
	 */
	boolean matches(Collection<T> objects);
	
	/**
	 * Return the {@link Collection} of matching objects.
	 * 
	 * @param objects<T> to match against this.
	 * @return the Collection<T> of objects matching.
	 */
	Collection<T> findMatches(T... objects);
	
	
	/**
	 * Return the {@link Collection} of matching objects.
	 * 
	 * @param objects {@link Collection}<T> to match against this.
	 * @return the Collection<T> of objects matching.
	 */
	Collection<T> findMatches(Collection<T> objects);

}
