package com.antropix.matcher.domain;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class User implements Serializable {
	
	private static final long serialVersionUID = 6454117836105097785L;

	private Map<String, String> attributesMap = new LinkedHashMap<String, String>(); 

	public enum Properties {
		
		FIRSTNAME("firstname"),
		SURNAME("surname"),
		ROLE("role"),
		AGE("age");
		
		private String name;
		
		private Properties(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public String getQuotedName() {
			return "\""+getName()+"\"";
		}
	}
	
	public enum Roles {
		
		ADMINISTRATOR("administrator"),
		DEVELOPER("developer"),
		MANAGER("manager"),
		DIRECTOR("director");
		
		private String name;
		
		private Roles(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public String getQuotedName() {
			return "\""+getName()+"\"";
		}
	}
	
	public User(String firstname,
				String surname,
				Roles administrator,
				String age) {
		setFirstname(firstname);
		setSurname(surname);
		setRole(administrator);
		setAge(age);
	}
	
	private void setFirstname(String firstname) {
		setProperty(Properties.FIRSTNAME, firstname);
	}

	public String getFirstname() {
		return getProperty(Properties.FIRSTNAME);
	}

	private void setSurname(String surname) {
		setProperty(Properties.SURNAME, surname);
	}
	
	public String getSurname() {
		return getProperty(Properties.SURNAME);
	}
	
	private void setRole(Roles role) {
		setProperty(Properties.ROLE, role.getName());
	}
	
	public String getRole() {
		return getProperty(Properties.ROLE);
	}
	
	private void setAge(String age) {
		setProperty(Properties.AGE, age);
	}
	
	public String getAge() {
		return getProperty(Properties.AGE);
	}

	public String getProperty(String key) {
		return attributesMap.get(key);
	}
	
	private String getProperty(Properties property) {
		return getProperty(property.getName());
	}

	private String setProperty(Properties property, String value) {
		return attributesMap.put(property.getName(), value);
	}

	@Override
	public String toString() {
		StringBuffer attributes = new StringBuffer();
		attributes.append("User [");
		for (String key : attributesMap.keySet()) {
			attributes.append(key + ": "+attributesMap.get(key) + ", ");
		}
		return attributes.append("].").toString();
	}
	
	
}
