package com.antropix.matcher.query;


import com.antropix.matcher.query.expression.Expression;
import com.antropix.matcher.query.predicate.Predicate;

/**
 * QueryTerm has either an {@link Expression} hence another sub-tree or has the
 * leafs key predicate values.
 * 
 * @author aheddad
 * 
 */
// TODO: divide in 2 implementations of Query instead, one is an Expression the
// other is the key predicate values
public class QueryTerm implements Query {

	private String stringRepresentation;

	private Expression expression;

	private Object key;
	private Predicate predicate;
	private Object values;

	public QueryTerm(String stringRepresentation) {
		setStringRepresentation(stringRepresentation);
	}

	public QueryTerm(Expression expression) {
		setExpression(expression);
	}

	public QueryTerm(Object key, Predicate predicate, Object value) {
		setKey(key);
		setPredicate(predicate);
		setValues(value);
	}

	// TODO: remove when dichotomy described in TODO above the class is done
	private boolean isValid() {
		return !(hasExpression() && hasPredicateLogic());
	}

	private boolean hasExpression() {
		return getExpression() != null;
	}

	private boolean hasPredicateLogic() {
		return getKey() != null || getPredicate() != null
				|| getValues() != null;
	}

	// Setters & getters

	protected String getStringRepresentation() {
		return stringRepresentation;
	}

	protected void setStringRepresentation(String stringRepresentation) {
		this.stringRepresentation = stringRepresentation;
	}

	protected Expression getExpression() {
		return expression;
	}

	// TODO: need to turn on asserts
	protected void setExpression(Expression expression) {
		if (isValid()) {
			this.expression = expression;
		} else {
			throw new InvalidQueryException(
					"Query has an expression and predicates");
		}
	}

	protected Object getKey() {
		return key;
	}

	protected void setKey(Object key) {
		if (isValid()) {
			this.key = key;
		} else {
			throw new InvalidQueryException(
					"Query has an expression and predicates");
		}
	}

	protected Predicate getPredicate() {
		return predicate;
	}

	protected void setPredicate(Predicate predicate) {
		if (isValid()) {
			this.predicate = predicate;
		} else {
			throw new InvalidQueryException(
					"Query has an expression and predicates");
		}
	}

	protected Object getValues() {
		return values;
	}

	protected void setValues(Object values) {
		if (isValid()) {
			this.values = values;
		} else {
			throw new InvalidQueryException(
					"Query has an expression and predicates");
		}
	}

}
