package com.antropix.matcher.query.expression;

import com.antropix.matcher.query.Query;
import com.antropix.matcher.query.operator.Operator;

/**
 * Represents an {@link Expression} composed by left and right operands 
 * with an operator in between f.ex.: A operator B, where (A, B) are {@link Query}.
 * 
 * @author aheddad
 *
 */
public class ComposedExpression implements Expression {

	private Query leftQuery;
	private Operator operator;
	private Query rightQuery;
	
	public ComposedExpression(Query leftQuery, Operator operator, Query rightQuery) {
		setLeftQuery(leftQuery);
		setOperator(operator);
		setRightQuery(rightQuery);
	}



	public Query getLeftQuery() {
		return leftQuery;
	}



	private void setLeftQuery(Query leftQuery) {
		this.leftQuery = leftQuery;
	}



	public Operator getOperator() {
		return operator;
	}



	private void setOperator(Operator operator) {
		this.operator = operator;
	}



	public Query getRightQuery() {
		return rightQuery;
	}



	private void setRightQuery(Query rightQuery) {
		this.rightQuery = rightQuery;
	}
	
	
	
}
