package com.antropix.matcher.query;

/**
 * Exception for invalid {@link Query}.
 * 
 * @author aheddad
 *
 */
public class InvalidQueryException extends RuntimeException {

	private static final long serialVersionUID = 4030005344821853301L;

	
	public InvalidQueryException(String message) {
		super(message);
	}


}
