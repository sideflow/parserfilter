package com.antropix.matcher.filter.configuration;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class XMLFilterConfigurationTest {

	private XMLFilterConfiguration config;
	
	@Before
	public void setUp() throws Exception {
		config = new XMLFilterConfiguration("src/test/resources/match_config.xml");
	}

	@Test
	public void test() {
		Assert.assertNotNull(config.getAndProperties());
	}

}
