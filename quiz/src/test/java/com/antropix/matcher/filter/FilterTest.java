package com.antropix.matcher.filter;

import java.util.Collection;

import junit.framework.Assert;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;

import com.antropix.matcher.domain.User;
import com.antropix.matcher.domain.User.Roles;
import com.antropix.matcher.filter.configuration.StringConfiguration;

public class FilterTest {
	
	private User joe;
	private User john;
	private User suzie;

	@Before
	public void setUp() throws Exception {
		joe = new User("Joe", "Bloggs", Roles.ADMINISTRATOR, "35");
		john = new User("John", "Bloggs", Roles.ADMINISTRATOR, "28");
		suzie = new User("Suzie", "Wu", Roles.ADMINISTRATOR, "49");
	}
	
	@Test
	public void testStringConfiguredFilterMatches() throws ConfigurationException {
		String query = "firstname=\"Joe\" and surname=\"Bloggs\" and role=\"administrator\" and age=\"35\"";
		StringConfiguration config = new StringConfiguration(query);
		Filter<User> filter = FilterFactory.createFilter(config);
		Assert.assertTrue(filter.matches(joe));
	}
	
	@Test
	public void testStringConfiguredFilterFind() throws ConfigurationException {
		String query = "surname=\"Bloggs\" and role=\"administrator\"";
		StringConfiguration config = new StringConfiguration(query);
		Filter<User> filter = FilterFactory.createFilter(config);
		Collection<User> matches = filter.findMatches(joe, john);
		Assert.assertEquals(2, matches.size());
		Assert.assertTrue(matches.contains(joe));
		Assert.assertTrue(matches.contains(john));
	}
	
	@Test
	public void testStringConfiguredSimpleQuery() throws ConfigurationException {
		String query = "role = \"administrator\" ";
		StringConfiguration config = new StringConfiguration(query);
		Filter<User> filter = FilterFactory.createFilter(config);
		Collection<User> matches = filter.findMatches(joe, john, suzie);
		Assert.assertEquals(3, matches.size());
		Assert.assertTrue(matches.contains(joe));
		Assert.assertTrue(matches.contains(john));
		Assert.assertTrue(matches.contains(suzie));
	}
	
	@Test
	public void testStringConfiguredLessThan() throws ConfigurationException {
		String query = "age < \"50\" ";
		StringConfiguration config = new StringConfiguration(query);
		Filter<User> filter = FilterFactory.createFilter(config);
		Collection<User> matches = filter.findMatches(joe, john, suzie);
		Assert.assertEquals(3, matches.size());
		Assert.assertTrue(matches.contains(joe));
		Assert.assertTrue(matches.contains(john));
		Assert.assertTrue(matches.contains(suzie));
	}
}
