package com.antropix.matcher;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.antropix.matcher.domain.UserTest;
import com.antropix.matcher.filter.FilterTest;
import com.antropix.matcher.query.QueryTermTest;
import com.antropix.matcher.query.parser.UQLParserTest;

/**
 * Test suite for project.
 * 
 * @author aheddad
 *
 */
@RunWith(Suite.class)
@SuiteClasses({
			UserTest.class, 
			FilterTest.class, 
			UQLParserTest.class, 
			QueryTermTest.class})
public class AllTests {

}
