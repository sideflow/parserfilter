package com.antropix.matcher.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.antropix.matcher.domain.User.Roles;

public class UserTest {

	private User joe;
	
	@Before
	public void setUp() throws Exception {
		joe = new User("Joe", "Bloggs", Roles.ADMINISTRATOR, "35");
	}

	@Test
	public void testUser() {
		Assert.assertNotNull(joe);
	}

	@Test
	public void testGetFirstname() {
		Assert.assertEquals("Joe", joe.getFirstname());
	}

	@Test
	public void testGetSurname() {
		Assert.assertEquals("Bloggs", joe.getSurname());
	}

	@Test
	public void testGetRole() {
		Assert.assertEquals("administrator", joe.getRole());
	}

	@Test
	public void testGetAge() {
		Assert.assertEquals("35", joe.getAge());
	}

}
