package com.antropix.matcher.query.parser;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.antropix.matcher.domain.User;
import com.antropix.matcher.domain.User.Roles;

public class UQLParserTest {

	private User joe, john;
	private String query;
	
	@Before
	public void setUp() throws Exception {
		joe = new User("Joe", "Bloggs", Roles.ADMINISTRATOR, "35");
		john = new User("John", "Doe", Roles.DEVELOPER, "34");
		query = "firstname=\"Joe\" and surname=\"Bloggs\" and role=\"administrator\" and age=\"35\"";
	}

	@Test
	public void testMatchesUser() throws ParseException {
		Assert.assertTrue("Expected true on match query against user.", UQLParser.matches(joe, query));
		Assert.assertFalse("Expected false on match query against user.", UQLParser.matches(john, query));
	}

	
}
