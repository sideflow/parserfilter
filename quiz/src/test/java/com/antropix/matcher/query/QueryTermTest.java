package com.antropix.matcher.query;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.antropix.matcher.query.expression.Expression;
import com.antropix.matcher.query.predicate.Is;
import com.antropix.matcher.query.predicate.Predicate;

public class QueryTermTest {

	private QueryTerm expressionQueryTerm;
	private QueryTerm predicateQueryTerm;
	
	private Expression expression;
	
	private Object key;
	private Predicate predicate;
	private Object value;
	
	@Before
	public void setUp() throws Exception {
		expressionQueryTerm = new QueryTerm(expression);
		key = new String("propertyName");
		value = new String("propertyValue");
		predicate = new Is();
		predicateQueryTerm = new QueryTerm(key, predicate, value);
	}

	@Test
	public void test() {
		Assert.assertNull(predicateQueryTerm.getExpression());
		Assert.assertNull(expressionQueryTerm.getPredicate());
	}

}
